start_code = "79-900"
end_code = "80-155"

def code_gen(start_code,end_code):
    code_list = []
    cond = "00-000"
    first = int(start_code[:2])
    second = int(start_code[3:])
    second_str='000'
    while cond != end_code:
        second+=1
        if len(str(second))==1:
            second_str = "00"+str(second)
        elif len(str(second))==2:
            second_str = "0"+str(second)
        else:
            second_str = str(second)
        code_list.append(str(first)+'-'+second_str)
        if second == 999:
            first+=1
            second=-1
        cond = (str(first)+'-'+second_str)
    return(code_list)

list_code = code_gen(start_code,end_code)
list_code
